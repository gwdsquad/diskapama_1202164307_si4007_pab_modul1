package com.example.diska_1202164307_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onclick (View view) {
        TextView alas = (TextView) findViewById(R.id.alas);
        TextView tinggi = (TextView) findViewById(R.id.tinggi);
        TextView hasil = (TextView) findViewById(R.id.hasil);

        int height = Integer.parseInt(tinggi.getText().toString());
        int base = Integer.parseInt(alas.getText().toString());
        int result = height * base;

        hasil.setText(String.valueOf(result+" cm"));
    }
}
